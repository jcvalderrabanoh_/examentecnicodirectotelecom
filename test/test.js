const { ExpectationFailed } = require('http-errors');
const request = require('supertest');
const app = require('../app');

describe('cadenaValida', () => {
  it('Deberia salir correcta', (done) => {
    request(app)
      .post('/busqueda')
      .send({ board: [["A", "B", "C", "E"], ["S", "F", "C", "S"], ["A", "D", "E", "E"]], word: "AABCCED" })
      .end(function (err, res) {
        done();
      });
  });
});

describe('cadenaInvalida', () => {
  it('Deberia salir incorrecta', (done) => {
    request(app)
      .post('/busqueda')
      .send({ board: [["A", "B", "C", "E"], ["S", "F", "C", "S"], ["A", "D", "E", "E"]], word: "AABCCEDx" })
      .expect(400)
      .end(function (err, res) {
          done();
      });
  });
});