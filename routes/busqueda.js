var express = require('express');
var router = express.Router();

/** libreria de utilerias nodejs */
var util = require('util')

/* GET users listing. */
router.post('/', function(req, res, next) {
    let data = req.body;
    let datosRequeridos= ['board','word'];
    let band=true;
    datosRequeridos.forEach((element)=>{
        if (!data[element]){
            band=false;
            return res.status(400).json({
                ok:false,
                mensaje: 'No cuenta con parametro '+ element
            });
        }
    });
    if (band){
        let board = data.board;
        let bandArreglo=false;
        let bandMatriz=false;
        bandArreglo = util.isArray(board);
        if (bandArreglo==false){
            return res.status(400).json({
                ok:false,
                mensaje: 'No es una matriz, no contiene dimensiones'
            });
        } else{
            bandMatriz = util.isArray(board[0]);
            if (bandMatriz==false){
                return res.status(400).json({
                    ok:false,
                    mensaje: 'No es una matriz, solo tiene una dimensión'
                });
            }else {
                let tmpCadena = "";
                board.forEach((arreglo, x)=>{
                    arreglo.forEach((valor, y)=>{
                        if (valor.length>1) {
                            return res.status(400).json({
                                ok:false,
                                mensaje : 'la matriz debe contener una letra o simbolo por celda, existe un error en la posición '+x.toString()+' : '+y.toString()
                            });
                            
                        }else{
                            tmpCadena+=valor;
                        }
                    });
                })
                let word = data.word;
                let bandEnc = false;
                let letrasSinEnc = "";
                for(var i =0; i<word.length; i++){
                    let isEnc = false;
                    let tmpCad = "";
                    for (var j = 0; j<tmpCadena.length; j++){
                        if ((word[i] == tmpCadena[j])&&isEnc==false){
                            isEnc = true;
                        }else {
                            tmpCad += tmpCadena[j];
                        }
                    }
                    if (isEnc==false){
                        letrasSinEnc +=word[i];
                    } else {
                        tmpCadena = tmpCad;
                    }
                }
                if (letrasSinEnc.length==0){
                    return res.json({
                        ok:true,
                        mensaje: 'Se encontraron todas las letras', 
                    });
                }else { 
                    return res.status(400).json({
                        ok:false,
                        mensaje: 'No se encontro la(s) letra(s) ' +letrasSinEnc,
                    });
                }

            }
        }
        
    }
    
    

    
  
});

module.exports = router;
